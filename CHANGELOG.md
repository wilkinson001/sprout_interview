# Sprout Blog and Content Moderation Platform Changelog

## Unreleased

### Added
* Initial repo setup
    * Changelog
    * Readme
    * Structure
    * Tooling
* Content Moderation API
    * POST `/sentences/`
* Tests for Content Moderation API
* Blog Posts API
    * POST `/posts/`
* Tests for Blog Posts API
* Dockerfile and docker-compose.yaml
* Requirements file
* Blog Posts Moderate API
    * POST `/posts/{post_id}/moderate/`
* Tests for Blog Posts Moderate API
* Tests for background tasks in Blog Posts App
* README

### Changed

* Altered tests directory structure