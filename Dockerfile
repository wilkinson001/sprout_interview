From python:3.9 as main

WORKDIR /code

COPY . /code

RUN pip install -r requirements.txt
