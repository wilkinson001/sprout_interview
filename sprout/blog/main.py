from os import environ
from typing import List, Optional

from fastapi import BackgroundTasks, Depends, FastAPI, status
from pydantic import BaseModel
from requests.exceptions import Timeout

import logging
import json
import requests

from sprout.shared.dependencies.common import application_json

CONTENT_MODERATION_URL = environ.get("CONTENT_MODERATION_URL")

app = FastAPI()

db = dict()


class BlogPostRequest(BaseModel):
    title: str
    paragraphs: List[str]


class BlogPost(BaseModel):
    title: str
    paragraphs: List[str]
    hasFoulLanguage: Optional[bool]


class BlogPostResponse(BaseModel):
    id: int
    code: int


def check_content(blog_post: int):
    headers = {"Content-Type": "application/json"}
    paragraphs_checked = list()

    for sentence in db[blog_post]["paragraphs"]:
        try:
            response = requests.post(
                CONTENT_MODERATION_URL + "/sentences/",
                data=json.dumps({"fragment": sentence}),
                headers=headers,
                timeout=5,
            )
            paragraphs_checked.append(json.loads(response.text)["hasFoulLanguage"])
        except Timeout:
            logging.warning(
                f"Could not contact Content Moderation API for Post {blog_post}",
            )
    if any(paragraphs_checked):
        db[blog_post]["hasFoulLanguage"] = True
    else:
        db[blog_post]["hasFoulLanguage"] = False


@app.post(
    "/posts/",
    dependencies=[Depends(application_json)],
    status_code=status.HTTP_201_CREATED,
)
def submit_blog_post(
    post: BlogPostRequest, background_tasks: BackgroundTasks
) -> BlogPostResponse:
    new_blog_key = max(db.keys() if db.keys() else [0]) + 1
    db[new_blog_key] = BlogPost(
        title=post.title, paragraphs=post.paragraphs, hasFoulLanguage=None
    ).dict()
    background_tasks.add_task(check_content, new_blog_key)
    return BlogPostResponse(id=new_blog_key, code=status.HTTP_201_CREATED)


class BlogModerationReponse(BaseModel):
    id: int
    code: int


@app.post("/posts/{post_id}/moderate/")
def moderate_blog_post(post_id: int) -> BlogModerationReponse:
    blog_post = db.get(post_id)
    if not blog_post:
        return BlogModerationReponse(id=post_id, code=status.HTTP_404_NOT_FOUND)
    check_content(post_id)
    return BlogModerationReponse(id=post_id, code=status.HTTP_202_ACCEPTED)
