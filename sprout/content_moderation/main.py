from csv import reader
from fastapi import Depends, FastAPI
from pydantic import BaseModel

from sprout.shared.dependencies.common import application_json

foul_words = set()


app = FastAPI()


@app.on_event("startup")
def startup_event():
    with open("sprout/content_moderation/foulwords.csv", "r") as words_file:
        csv_reader = reader(words_file, delimiter=",")
        for row in csv_reader:
            for word in row:
                foul_words.add(word)


class Content(BaseModel):
    fragment: str


class ContentReturn(BaseModel):
    hasFoulLanguage: bool


@app.post("/sentences/", dependencies=[Depends(application_json)])
def post_sentences(content: Content) -> ContentReturn:
    return ContentReturn(
        hasFoulLanguage=any(
            word in content.fragment.lower().split() for word in foul_words
        )
    )
