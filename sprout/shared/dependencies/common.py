from fastapi import Header, HTTPException, status


def application_json(content_type: str = Header(...)):
    if content_type != "application/json":
        raise HTTPException(
            status.HTTP_415_UNSUPPORTED_MEDIA_TYPE,
            "API only accepts `application/json` content_types",
        )
