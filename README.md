# Sprout App

The Sprout app comprises of two seperate API's, one which accepts submitted blog posts, and another which moderates content of blog posts for foul language.

## Running

You can run the services using the following commands:
```
docker-compose build
docker-compose up -d
```

You can then access both services on the following ports:

* Blog API - 5000
* Content Moderation API - 5001

## Tests

To run the tests for this repo, run the following command:

```
PYTHONPATH=. CONTENT_MODERATION_URL="localhost" pytest tests
```