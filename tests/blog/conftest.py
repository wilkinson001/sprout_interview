from fastapi.testclient import TestClient
from unittest import mock

from sprout.blog.main import app

import pytest
import json


class MockResponse:
    def __init__(self, response):
        self.response = response
        self.text = json.dumps(response)

    def __call__(self, *args, **kwargs):
        return self


@pytest.fixture()
def mock_db():
    with mock.patch("sprout.blog.main.db", dict()) as db:
        yield db


@pytest.fixture(scope="session")
def test_client():
    with TestClient(app) as client:
        yield client
