from unittest import mock

import pytest
from requests import Timeout

from sprout.blog.main import check_content
from tests.blog.conftest import MockResponse


CHECK_CONTENT_PARAMS = [
    pytest.param(
        1,
        False,
        {"paragraphs": ["word"]},
        {"paragraphs": ["word"], "hasFoulLanguage": False},
        id="check_content_no_foul_words",
    ),
    pytest.param(
        2,
        True,
        {"paragraphs": ["foul_word"]},
        {"paragraphs": ["foul_word"], "hasFoulLanguage": True},
        id="check_content_foul_words",
    ),
]


@pytest.mark.parametrize("blog_id,foul_word,data,expected_data", CHECK_CONTENT_PARAMS)
def test_check_content(blog_id, foul_word, data, expected_data, mock_db):
    with mock.patch(
        "sprout.blog.main.requests.post", MockResponse({"hasFoulLanguage": foul_word})
    ):
        mock_db[blog_id] = data
        check_content(blog_id)
        assert mock_db[blog_id] == expected_data


def test_check_content_timeout(mock_db, caplog):
    mock_db[1] = {"paragraphs": [""]}
    with mock.patch("sprout.blog.main.requests.post", mock.Mock(side_effect=Timeout())):
        check_content(1)
    assert "Could not contact Content Moderation API for Post 1" in caplog.text
