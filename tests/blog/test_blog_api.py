import pytest
import json
from unittest import mock
from tests.blog.conftest import MockResponse


POSTS_PARAMS = [
    pytest.param(
        {"title": "foo", "paragraphs": ["foo", "bar"]},
        {"id": 1, "code": 201},
        {"title": "foo", "paragraphs": ["foo", "bar"], "hasFoulLanguage": False},
        False,
        id="good_blog_post_no_foul_words",
    ),
    pytest.param(
        {"title": "foo", "paragraphs": ["foo", "plonker"]},
        {"id": 1, "code": 201},
        {"title": "foo", "paragraphs": ["foo", "plonker"], "hasFoulLanguage": True},
        True,
        id="good_blog_post_with_foul_words",
    ),
]


@pytest.mark.parametrize("data,expected_response,expected_data,foul_word", POSTS_PARAMS)
def test_post_posts(
    data, expected_response, expected_data, foul_word, test_client, mock_db
):
    with mock.patch(
        "sprout.blog.main.requests.post", MockResponse({"hasFoulLanguage": foul_word})
    ):
        headers = {"Content-Type": "application/json"}
        response = test_client.post("/posts/", data=json.dumps(data), headers=headers)
        assert json.loads(response.text) == expected_response
        assert mock_db[1] == expected_data


MODERATE_PARAMS = [
    pytest.param(
        1,
        False,
        {"id": 1, "code": 202},
        {"title": "foo", "paragraphs": ["bar"], "hasFoulLanguage": False},
        id="good_post_no_foul_words",
    ),
    pytest.param(
        10,
        True,
        {"id": 10, "code": 202},
        {"title": "foo", "paragraphs": ["bar", "plonker"], "hasFoulLanguage": True},
        id="good_post_foul_words",
    ),
    pytest.param(
        100,
        True,
        {"id": 100, "code": 404},
        None,
        id="bad_post",
    ),
]


@pytest.mark.parametrize(
    "post_id,foul_word,expected_response,expected_data", MODERATE_PARAMS
)
def test_post_moderate(
    post_id, foul_word, expected_response, expected_data, test_client, mock_db
):
    mock_db[post_id] = expected_data
    with mock.patch(
        "sprout.blog.main.requests.post", MockResponse({"hasFoulLanguage": foul_word})
    ):
        headers = {"Content-Type": "application/json"}
        response = test_client.post(f"/posts/{post_id}/moderate/", headers=headers)
        assert json.loads(response.text) == expected_response
        assert mock_db.get(post_id) == expected_data


CONTENT_TYPE_PARAMS = [
    pytest.param({"Content-Type": "application/json"}, 201, id="good_content_type"),
    pytest.param({"Content-Type": "application/text"}, 415, id="bad_content_type"),
]


@pytest.mark.parametrize("headers,expected_status", CONTENT_TYPE_PARAMS)
def test_post_posts_content_types(headers, expected_status, test_client):
    response = test_client.post(
        "/posts/",
        data=json.dumps({"title": "foo", "paragraphs": []}),
        headers=headers,
    )
    assert response.status_code == expected_status
