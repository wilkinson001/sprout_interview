from fastapi.testclient import TestClient
from sprout.content_moderation.main import app

import pytest


@pytest.fixture(scope="session")
def test_client():
    with TestClient(app) as client:
        yield client
