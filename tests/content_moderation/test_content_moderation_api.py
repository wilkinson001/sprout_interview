import pytest
import json

SENTENCES_PARAMS = [
    pytest.param({"fragment": "foo"}, {"hasFoulLanguage": False}, id="non_foul_word"),
    pytest.param(
        {"fragment": "plonker"}, {"hasFoulLanguage": True}, id="single_foul_word"
    ),
    pytest.param(
        {"fragment": "foo bar fizz buzz"},
        {"hasFoulLanguage": False},
        id="no_foul_words_full_sentence",
    ),
    pytest.param(
        {"fragment": "24 carat plonker"},
        {"hasFoulLanguage": True},
        id="single_foul_word_full_sentence",
    ),
    pytest.param(
        {"fragment": "rubbish 24 carat plonker"},
        {"hasFoulLanguage": True},
        id="multi_foul_word",
    ),
    pytest.param(
        {"fragment": "RubbisH 24 carat pLonKer"},
        {"hasFoulLanguage": True},
        id="foul_word_casing",
    ),
]


@pytest.mark.parametrize("data,expected_response", SENTENCES_PARAMS)
def test_post_sentences(data, expected_response, test_client):
    headers = {"Content-Type": "application/json", "accept": "application/json"}
    response = test_client.post("/sentences/", data=json.dumps(data), headers=headers)
    assert json.loads(response.text) == expected_response


CONTENT_TYPE_PARAMS = [
    pytest.param({"Content-Type": "application/json"}, 200, id="good_content_type"),
    pytest.param({"Content-Type": "application/text"}, 415, id="bad_content_type"),
]


@pytest.mark.parametrize("headers,expected_status", CONTENT_TYPE_PARAMS)
def test_sentances_content_type_header(headers, expected_status, test_client):
    response = test_client.post(
        "/sentences/", data=json.dumps({"fragment": "foo"}), headers=headers
    )
    assert response.status_code == expected_status
